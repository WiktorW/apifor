﻿using Microsoft.EntityFrameworkCore;
using WebApplication.Models;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace WebApplication
{
    public class ContextCars : DbContext
    {
        public ContextCars(DbContextOptions<ContextCars> options) : base(options)
        {
        }
        public DbSet<Car> Cars { get; set; }
    }
}