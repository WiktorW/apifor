﻿using System;

namespace WebApplication.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProductionDate { get; set; }
        public string Producer { get; set; }
        public string CarModel { get; set; }
        public decimal Price { get; set; }
    }
}