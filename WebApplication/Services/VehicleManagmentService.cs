﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models;

namespace WebApplication.Services
{
    public class VehicleManagmentService
    {
        private ContextCars _contextCars;

        public VehicleManagmentService(ContextCars cars)
        {
            _contextCars = cars;
        }

        public async Task RemoveProduct(int iD)
        {
            try
            {
                var vehicle = _contextCars.Cars.FirstOrDefault(x => x.Id == iD);
                _contextCars.Remove(vehicle);
                await _contextCars.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task AddProduct(string nazwa, string dataWydania, string markaSamochodu, string modelSamochodu,
            decimal cena)
        {
            _contextCars.Add(new Car()
            {
                Name = nazwa,
                ProductionDate = dataWydania,
                Producer = markaSamochodu,
                CarModel = modelSamochodu,
                Price = cena
            });

            await _contextCars.SaveChangesAsync();
        }

        public async Task<List<Car>> GetProducts()
        {
            var cars = await _contextCars.Cars.ToListAsync();
            return cars;
        }

        public async Task<Car> FindProductById(int id)
        {
            var car = await _contextCars.Cars.FirstAsync(x => x.Id == id);
            return car;
        }

        public async Task ChangePrices(int iD, decimal price)
        {
            try
            {
                _contextCars.Cars.FirstAsync(x => x.Id == iD).Result.Price = price;
                await _contextCars.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}