﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using WebApplication.Services;

namespace WebApplication.Controllers
{
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private VehicleManagmentService _vehicleManagmentService;
        public ProductsController(VehicleManagmentService vehicleManagmentService)
        {
            _vehicleManagmentService = vehicleManagmentService;
        }
        [HttpGet,Route("api/controller/getCarById")]
        public async Task<ActionResult<Car>> GetCarById(Car options)
        {
           return await _vehicleManagmentService.FindProductById(options.Id);
        }

        [HttpPost, Route("api/controller/addCar")]
        public async Task<ActionResult> AddCar(Car options)
        {
            await _vehicleManagmentService.AddProduct(options.Name, options.ProductionDate, options.Producer,
                options.CarModel, options.Price);
            return Ok();
        }

        [HttpPost, Route("api/controller/removeCar")]
        public async Task<ActionResult> RemoveCar(Car options)
        {
            await _vehicleManagmentService.RemoveProduct(options.Id);
            return Ok();
        }

        [HttpGet, Route("api/controller/getCars")]
        public async Task<ActionResult<List<Car>>> GetCars()
        {
            var result = await _vehicleManagmentService.GetProducts();
            return result;
        }

        [HttpPost, Route("api/controller/changePrices")]
        public async Task<ActionResult> ChanePricing(Car options)
        {
            await _vehicleManagmentService.ChangePrices(options.Id, options.Price);
            return Ok();
        }
    }
}